# EyE

Shows an looking eye on the flow3r badge

inspired by mk's gifplayer

thanks to @schneider for making this app firmware v1.3 ready

## Install

Goto  https://flow3r.garden/apps/ or
download the repo and copy over to `apps` on your flow3r.

You can generate frames from your own gif using ffmpeg
```
ffmpeg -i rick.gif -vf scale=240:240 frames/frame-%d.png
```
