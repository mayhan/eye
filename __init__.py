from st3m.application import Application, ApplicationContext
import st3m.run
import leds
import os
import sys
import time


class EyE(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.current_frame = 0
        self.max_frames = 71

    def draw(self, ctx: Context) -> None:
        ctx.image_smoothing = False
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Loop if we printed the last frame
        if self.current_frame > self.max_frames:
                self.current_frame = 0
                
        # slow down the framerate a bit
        time.sleep_ms(80)

        # Draw frame
        path = f"{self.app_ctx.bundle_path}/frames/frame-{self.current_frame}.png"
        ctx.image(path,-120,-120,240,240)

        # Increase frame
        self.current_frame += 1
    
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        leds.set_all_rgb(0, 0, 255)
        leds.update()


if __name__ == '__main__':
    st3m.run.run_view(EyE(ApplicationContext()))
